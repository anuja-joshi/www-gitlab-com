---
layout: handbook-page-toc
title: "Field Operations"
description: "The Field Operation team's vision is to manage field business processes, systems, architecture, enablement, champion data integrity, provide insights and predictability through analytics"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Welcome to the Field Operations Handbook

“Manage field business processes, systems, architecture, enablement, champion data integrity, provide insights and predictability through analytics”

The term *"sales"* refers to the Sales Team and *"field"* includes the Customer Success Team. 

### Key Tenets
**Clarity**: for definitions, processes and events   
**Visibility**: to processes, data and analytics   
**Accountability**: for both Sales/Field Ops to uphold to expectations and SLAs   

### Teams
* [Sales Operations](/handbook/sales/field-operations/sales-operations/)
    *   [Deal Desk](/handbook/sales/field-operations/sales-operations/deal-desk/#welcome-to-the-deal-desk-handbook)
    *   [Customer Success Operations](/handbook/sales/field-operations/customer-success-operations)
    *   [Sales Commissions](/handbook/sales/commissions/)
* [Sales Systems](/handbook/sales/field-operations/sales-systems/)
* [Sales Strategy](/handbook/sales/field-operations/sales-strategy/) 
* [Field Enablement](/handbook/sales/field-operations/field-enablement/)
* [Channel Operations](/handbook/sales/field-operations/channel-operations/)
