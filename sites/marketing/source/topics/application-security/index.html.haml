---
layout: default
title: What is Application Security
description: "Effectively turn your DevOps methodology into a DevSecOps methodology"
suppress_header: true
extra_css:
  - devops.css
  - agile-delivery.css
  - application-security.css
  - cta-promo.css
extra_js:
  - libs/on-scroll.js
  - all-clickable.js
canonical_path: "/topics/application-security/"
sidebar_nav: true
sidebar_nav_title: "Related articles"
sidebar_nav_links:
  - title: 'What is fuzz testing?'
    url: '/topics/application-security/what-is-fuzz-testing/'
  - title: 'What is developer-first security?'
    url: '/topics/application-security/what-is-developer-first-security.index.html'
---
.blank-header
  = image_tag "/images/home/icons-pattern-left.svg", class: "image-border image-border-left", alt: "Gitlab hero border pattern left svg"
  = image_tag "/images/home/icons-pattern-right.svg", class: "image-border image-border-right", alt: "Gitlab hero border pattern right svg"
  .header-content
    = image_tag "/images/application-security.svg", class: "hero-image-huge", alt: "GitLab DevSecOps graphic"
    %h1 Application Security
    %p Can your existing application security keep pace with modern development methods? Learn how next-generation software requires a new approach to app sec.
    = link_to "Download the ebook", "/resources/ebook-ciso-secure-software/", class: "btn cta-btn orange devops-cta"

.container.wrapper.wrapper--large-fonts
  #content.devops-content.u-margin-top-md.row
    - localvar_sidebar_enabled = current_page.data.sidebar_nav || false
    - if localvar_sidebar_enabled == true
      .col-md-2#sidebar-nav
        = partial "/includes/components/sidebar-nav"
    - if localvar_sidebar_enabled == true
      - localvar_sidebar_class = 'col-md-10'
    - else
      - localvar_sidebar_class = 'col-md-12'
    %div{class: "#{localvar_sidebar_class}"}
      .wrapper.js-in-page-nav-group{role: "main"}
        .row.u-margin-top-lg.js-in-page-nav-section#what-is-application-security
          .col-md-12
            %h2.u-text-brand What is application security?

            %p Application security has been <a href="https://searchsoftwarequality.techtarget.com/definition/application-security">defined by TechTarget</a> as the use of software, hardware, and procedural methods to protect applications from external threats. Modern approaches include shifting left to find and fix vulnerabilities earlier, while also shifting right to protect your applications and their infrastructure-as-code in production. Securing the Software Development Life Cycle (SDLC) itself is often a requirement as well. 

            %p This approach of building security into your development and operational processes effectively turns your DevOps methodology into a DevSecOps methodology. An end-to-end DevSecOps platform can best enable this approach. 

        .row.u-margin-top-lg.js-in-page-nav-section#why-adopt-devsecops
          .col-md-12
            %h2.u-text-brand Why adopt DevSecOps?

            %p Security has traditionally come at the end of the development lifecycle, adding cost and time when code is inevitably sent back to the developer for fixes. It is time to shift left by embracing security earlier within the DevOps lifecycle. 
            
            = image_tag "/images/devsecops/cost-savings-to-shift-left.png", class: "u-margin-bottom-sm"

            %p DevSecOps tools automate security workflows to create an adaptable process for your development and security teams, improving collaboration and breaking down silos. By embedding security into the SDLC, you can consistently secure fast- moving and iterative processes - improving efficiency without sacrificing quality.

            %p DevSecOps weaves security practices into every stage of software development right through deployment with the use of tools and methods to protect and monitor live applications. New attack surfaces such as containers and orchestrators must be monitored and protected alongside the application itself.

            %div.u-image-bg.u-text-light.u-margin-top-xl.u-margin-bottom-md{ style: "background-image:url('/images/scaling-devops-bg.jpg');" }
              .row.u-margin-top-lg.u-margin-bottom-lg{ style: "padding: 40px;" }
                .col-md-12
                  %h2 Manage your toolchain before it manages you

                  %p.u-margin-top-xs Visible, secure, and effective toolchains are difficult to come by due to the increasing number of tools teams use, and it’s placing strain on everyone involved. This study dives into the challenges, potential solutions, and key recommendations to manage this evolving complexity.

                  .btn-group
                    %a.btn.cta-btn.purple-reverse{ href: "https://about.gitlab.com/resources/whitepaper-forrester-manage-your-toolchain/" } Read the study

        .row.u-margin-top-lg.js-in-page-nav-section#benefits-of-devsecops
          .cta-featured-grid
            .row.u-margin-bottom-md
              .col-md-10.col-md-offset-1
                %h2.u-text-brand Benefits of DevSecOps
                .featured-icons.text-center
                  .featured-icon
                    = partial "/includes/icons/time-icon2.svg"
                    %p.number Speed
                    %p.description Developers can remediate vulnerabilities while they’re coding, which teaches secure code writing and reduces back and forth during security reviews.
                  .featured-icon
                    = partial "/images/icons/first-look-influence.svg"
                    %p.number Collaboration
                    %p.description Encouraging a security mindset across your app dev team aligns goals with security and encourages employees to work with others outside their functional silo.
                  .featured-icon
                    = partial "/images/icons/solid-icons/trending-icon2.svg"
                    %p.number Efficiency
                    %p.description DevSecOps saves time, money, and employee resources over every launch and iteration - all valuable assets to IT and security teams suffering from <a href="https://go.forrester.com/blogs/security-risk-2019-cybersecuritys-staffing-shortage-is-self-inflicted/">skills</a> and budget shortages.


        .row.u-margin-top-sm.js-in-page-nav-section#devsecops-fundamentals
          .col-md-12
            %h2.u-text-brand DevSecOps fundamentals

            %p If you've read the book that was the genesis for the DevOps movement, The Phoenix Project, you understand the importance of automation, consistency, metrics, and collaboration. For DevSecOps, you are essentially applying these techniques to outfit the software factory while embedding security capabilities along the way rather than in a separate, siloed process. Dev or security can find vulnerabilities, but a developer is usually required to remove such flaws. It makes sense to empower them to find and fix vulnerabilities while they are still working on the code. Scanning alone isn't enough. It's about getting the results to the right people, at the right time, with the right context for quick action.
            
            %p Fundamental requirements include automation and collaboration, along with policy guardrails and visibility.  

            %h3.u-margin-top-0 Automation

            %p <a href="https://about.gitlab.com/developer-survey/">According to our 2019 Developer Survey</a>, most developers test <em>less than half</em> of their code with automated application security methods. The code that is tested is most commonly reviewed with the following:

            = image_tag "/images/application-security-survey-graph.png", class: "u-margin-bottom-sm"

            %p There is always more to be done when it comes to testing. It’s best to understand which tests work best for you (this can depend on app or software function, development processes, infrastructure, etc.), and incorporate those into your DevSecOps practice.

            %h3.u-margin-top-0 Collaboration 

            %p A single source of truth that reports vulnerabilities and remediation provides much-needed transparency to both development and security team. It can streamline cycles, eliminate friction, and remove unnecessary translation across tools. 

            %h3.u-margin-top-0 Policy guardrails

            %p Every enterprise has a different appetite for risk. Your security policies will reflect what is right for you while the regulatory requirements to which you must adhere will also influence the policies you must apply. Hand-in-hand with automation, guardrails can ensure consistent application of your security and compliance policies.

            %h3.u-margin-top-0 Visibility 

            %p An end-to-end DevSecOps platform can give auditors a clear view into who changed what, where, when, and why from beginning to end of the software lifecyle. Leveraging a single-source-of-truth can also ensure earlier visibility into application risks.

        .row.u-margin-top-lg.js-in-page-nav-section#resources
          .col-md-12

            %h2.u-text-brand Getting started

            %p Ready to see how GitLab can help you get started with DevSecOps? 
            
            %p Our <a href="https://about.gitlab.com/solutions/dev-sec-ops/">DevSecOps Solution</a> page has all of the details, along with a Free Trial offer for our Ultimate tier of capabilities. 

            %div.u-image-bg.u-text-light.u-margin-top-xl.u-margin-bottom-md{ style: "background-image:url('/images/scaling-devops-bg.jpg');" }
              .row.u-margin-top-lg.u-margin-bottom-lg{ style: "padding: 40px;" }
                .col-md-12
                  %h2 Assess your DevSecOps maturity

                  %p.u-margin-top-xs We have developed a simple assessment that uses twenty questions to help you determine where you excel and areas for improvement. A helpful guide suggests steps to get you started.

                  .btn-group
                    %a.btn.cta-btn.purple-reverse{ href: "https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/design/publications/e-books/security-maturity-model/pdf/security-maturity-model.pdf" } Assess yourself

            %h3.u-margin-top-0 Resources

            %p Here is a list of resources on DevSecOps that we find to be particularly helpful. We would love to get your recommendations on books, blogs, videos, podcasts and other resources that offer valuable insight into best practices.

            %p
              Please share your favorites with us by tweeting us
              %a{:href => 'https://twitter.com/gitlab'} @GitLab!

            .feature-group.feature-group--alt.u-margin-top-lg.u-padding-top-0.u-padding-bottom-0
              .row.flex-row
                .col-md-4.col-lg-4.u-margin-bottom-sm
                  .feature.js-all-clickable
                    .feature-media
                      = image_tag "/images/feature-thumbs/feature-thumb-workflow.jpg"
                    .feature-body
                      %h3.feature-title Why you need static and dynamic application security testing in your development workflows
                      %p.feature-description Bolster your code quality with static and dynamic application security testing.
                      = link_to "Read", "/blog/2019/08/12/developer-intro-sast-dast/", class: "feature-more"

                .col-md-4.col-lg-4.u-margin-bottom-sm
                  .feature.js-all-clickable
                    .feature-media
                      = image_tag "/images/feature-thumbs/feature-thumb-4-ways-developers-secure.jpg"
                    .feature-body
                      %h3.feature-title 4 Ways developers can write secure code with GitLab
                      %p.feature-description GitLab Secure is not just for your security team - it’s for developers too.
                      = link_to "Read", "/blog/2019/09/03/developers-write-secure-code-gitlab/", class: "feature-more"

                .col-md-4.col-lg-4.u-margin-bottom-sm
                  .feature.js-all-clickable
                    .feature-media
                      = image_tag "/images/feature-thumbs/feature-thumb-5-security-testing-principles.jpg"
                    .feature-body
                      %h3.feature-title 5 Security testing principles every developer should know
                      %p.feature-description Developers are looking for guidance and standard practices as they take on more security testing responsibilities.
                      = link_to "Read", "/blog/2019/09/16/security-testing-principles-developer/", class: "feature-more"

        = partial "/includes/ctas/cta-promo"
