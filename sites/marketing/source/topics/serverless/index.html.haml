---
title: What is serverless?
description: "Serverless software architecture uses cloud managed services and event driven code, allowing developers to build scalable and cost-efficient applications"
canonical_path: "/topics/serverless/"
suppress_header: true
extra_css:
  - auto-devops.css
  - product.css
  - resources.css
extra_js:
  - features.js
---

.blank-header
  = image_tag "/images/home/icons-pattern-left.svg", class: "image-border image-border-left", alt: "Gitlab hero border pattern left svg"
  = image_tag "/images/home/icons-pattern-right.svg", class: "image-border image-border-right", alt: "Gitlab hero border pattern right svg"
  .header-content
    = image_tag "/images/devops-tools/gitlab-logo.svg", class: "hero-image-small", alt: "Gitlab logo svg"
    %h1 What is serverless?
    %p
      Serverless is a software architecture design pattern that takes advantage of event-driven code execution powered by cloud managed services to build massively scalable and cost-efficient applications composed of small discrete functions without developers needing to design for or think about the underlying infrastructure where their code runs.
    = link_to "Learn more about GitLab", "/why/", class: "btn cta-btn accent"

.toc-links
  = link_to "Serverless business logic", "#serverless-business-logic"
  = link_to "FaaS and managed services", "#serverless-faas-functions-as-a-service-and-managed-services"
  = link_to "Attributes of serverless", "#attributes-of-serverless"
  = link_to "Cloud managed services", "#comparison-of-cloud-managed-services"
  = link_to "Business value of serverless", "#business-value-of-serverless"
  = link_to "Benefits of GitLab Serverless", "#benefits-of-gitlab-serverless"
  = link_to "Additional resources", "#additional-resources"

.content-container
  .content.tile
    :markdown
      ## Serverless business logic

      What is serverless? Every application uses servers at some point. The term Serverless emphasizes an architecture and service model where the developers need not concern themselves with infrastructure and instead can focus on the business logic of their appliction. Serverless is the next evolution of architectural design from monolith, to [microservices](/topics/microservices/), to functions as Adrian Cockcroft explains in this video:

    %p
      <iframe width="853" height="480" src="https://www.youtube-nocookie.com/embed/aBcG57Gw9k0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

  .content.tile
    :markdown
      ## Serverless, FaaS (Functions as a service), and managed services

      ![monolith vs microservices](/images/serverless/serverless-header.svg)

      Often serverless and FaaS are treated as interchangeable terms, but this isn't really accurate. Serverless is an overarching architectural pattern that makes use of a FaaS along with other cloud managed services. FaaS is a specific type of service such as AWS Lambda, Google Cloud Functions, and Azure Functions, that enables developers to deploy functions.

  .content.tile
    :markdown
      ## Attributes of serverless

      1. Small, discrete units of code. Often services written using serverless architecture are comprised of a single function.
      2. Event-based execution. The infrastructure needed to run a function doesn't exist until a function is triggered. Once an event is received an ephemeral compute environment is created to execute that request. The environment may be destroyed immediately, or more commonly stays active for a short period of time, commonly 5 minutes.
      3. Scale to zero. Once a function stops receiving requests the infrastructure is taken down and completely stops running. This saves on cost since the infrastructure only runs when there is usage. If there's no usage, the environment scales down to zero.
      4. Scale to infinity. The FaaS takes care of monitoring load and creating additional instances when needed, in theory, up to infinity. This virtually eliminates the need for developers to think about scale as they design applications. A single deployed function can handle one or one billion requests without any change to the code.
      5. Use of managed services. Often serverless architectures make use of cloud provided services for elements of their application that provide non-differentiated heavy lifting such as file storage, databases, queueing, etc. For example, Google's Firebase is popular in the serverless community as a database and state management service that connects to other Google services like Cloud Functions.

  .content.tile
    :markdown
      ## Comparison of cloud managed services

      Here is a chart of with examples of managed servies from AWS, Google Cloud, and Azure along with their open source counterparts.

      | Service               | Open Source           | AWS                   | Google Cloud          | Azure                 |
      | --------------------- | --------------------- | --------------------- | --------------------- | --------------------- |
      | FaaS                  | Knative               | Lambda                | Cloud Functions       | Azure Functions       |
      | Storage               | Minio                 | S3                    | Cloud storage         | Azure Storage         |
      | SQL DB                | MySQL                 | RDS                   | Cloud SQL             | Azure SQL Database    |
      | NoSQL DB              | MongoDB, Cassandra, CouchDB | DynamoDB        | Cloud Datastore       | Cosmos DB             |
      | Message queue         | Kafka, Redis, RabbitMQ | SQS, Kinesis         | Google Pub/Sub        | Azure Queue Storage   |
      | Service mesh          | Istio                 | App Mesh              | Istio on GKE          | Azure Service Fabric Mesh |

  .content.tile
    :markdown
      ## Business value of serverless

      1. Faster pace of innovation. Developer productivity increases when they can focus solely on business logic.
      1. Greater stability/resiliency (less loss of revenue due to downtime)
      1. Greater scale, the software is able to keep up with business demand
      1. Lower costs. Since compute is only billed when a service is active, servless provides tremendous cost savings vs always-on infrastructure.

  .content.tile
    :markdown
      ## Benefits of GitLab Serverless

      [GitLab Serverles](https://about.gitlab.com/product/serverless/) allows business to deploy their own FaaS on Kubernetes.

      1. No vendor lock-in. Organizations can choose who they want to run their compute. In any cloud that supports Kubernetes, or even on-premises servers.
      1. Your FaaS is part of the same workflow as the rest of your software lifecyle with a single appliction from planning and testing, to deployment and monitoring.
      1. Deploying functions is greatly streamlined and simplified vs using Knative directly.

  .content.tile
    :markdown
      ## Additional resources
      - [What is Serverless Architecture? What are its Pros and Cons?](https://hackernoon.com/what-is-serverless-architecture-what-are-its-pros-and-cons-cc4b804022e9)
      - [Knative](https://cloud.google.com/knative/)
      - [Martin Folwer on serverless architectures](https://martinfowler.com/articles/serverless.html)


